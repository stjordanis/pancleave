import pandas as pd
from encode import Encoder
from fragment import Fragmenter
from classify import Classifier
from utils import Utils


class Pipeline:
    
    
    '''
    This class provides convenient functionality for a full 
    "computational protease" pipeline. Class dependencies are 
    Encoder (encode module), Fragmenter (fragment module), and 
    Classifier (classify module).
    '''
    
    
    def execute_pipeline(self, 
                         seq_list, 
                         confidence = None, 
                         aa_code = "one"):
        
        '''
        This method executes the entire panCleave pipeline, from sequence
        encoding to cleavage site prediction and putative encrypted peptide 
        fragmentation.
        
        Parameters:
            seq_list: a list of strings representing initial protein sequences 
                in one-letter amino acid code.
            confidence (default = None): a float or None-type indicating the
                desired confidence threshold for class label probability
                estimates. If the user does not wish to filter predicted
                cleavage sites by model confidence (i.e. probability threshold),
                pass None. Else, set value to < 0.5 and >= 1. Filtering by
                threshold will only return cryptides resulting from cleavages
                of the given probability threshold or higher. This parameter must
                be set to None if probability estimates are not returned by 
                the model.
            aa_code (default = "one"): string indicating format of original
                input sequences. String "one" indicates one-letter amino acid
                code, "three" indicates three-letter code, and "full" indicates
                full residue names. Any input other than "one" requires conversion
                to one-letter code, which is done internally.
                
        Return:
            Pandas dataframe 
        '''
        
        # Instantiate model, sequence fragmenter, and encoder objects.
        panCleave = Classifier()
        frag = Fragmenter()
        fp = Encoder("protfp")
        
        # Init data structures.
        predictions = []
        probabilities = []
        fragments = []
        
        # Convert to one-letter amino acid code, if necessary.
        if (aa_code == "three"):
            seq_list = fp.singlefy_list(seq_list, input_code = "three")
        elif (aa_code == "full"):
            seq_list = fp.singlefy_list(seq_list, input_code = "full")
        
        # Remove all invalid sequences.
        seq_list = [seq for seq in seq_list if fp.test_valid(seq, 
                                                             desired_len = 8, 
                                                             threshold = "min")]
        
        # Compute all potential 8-residue cleavage sites.
        sites = frag.slide_window(seq_list)
        
        # Predict label for each potential site.
        for subsites in sites:
            pred, pr = panCleave.classify(subsites, return_type = "list")
            predictions.append(pred)
            probabilities.append(pr)
            
        # Cleave fragments using predicted label and probability (if relevant).
        for i in range(len(seq_list)):
            if (("Positive" not in predictions[i]) or (len(seq_list[i]) < 8)):
                cryptides = None
            else:
                site_filter = [(item == "Positive") for item in predictions[i]]
                if (confidence != None):
                    pr_mask = [(item >= confidence) for item in probabilities[i]]
                    site_filter = [all(mask) for mask in zip(site_filter, pr_mask)]
                filtered_sites = [sites[i][j] for j in range(len(sites[i])) if site_filter[j]]
                cryptides = frag.cleave_single(seq_list[i], filtered_sites)
                if (len(cryptides) == 0):
                    cryptides = None
            
            fragments.append(cryptides)
        
        # Construct and return results dataframe.
        df = pd.DataFrame(zip(seq_list, sites, predictions, 
                              probabilities, fragments),
                          columns = ["Sequence", "Sites", "Predictions", 
                                     "Probabilities", "Fragments"])
        return df
    
    
    def filter_results(self,
                       fragments, 
                       min_len = None, 
                       max_len = None, 
                       verbose = False):

        '''
        This method filters results from the panCleave pipeline by
        dropping duplicates and removing protein fragments longer than
        a set threshold (if desired).

        Parameters:
            fragments: iterable of fragments returned by Pipeline.execute_pipeline(),
                e.g. df["Fragments"].
            min_len (default = None): integer indicating minimum allowable
                fragment length. If value is None, no threshold will be set.
            max_len (default = None): integer indicating maximum allowable
                fragment length. If value is None, no threshold will be set.
            verbose (default = False): boolean indicating whether to print
                descriptive data for results.

        Return: list of filtered results.
        '''

        # Analyze results.
        filtered_frags = [sublist for sublist in fragments if (sublist != None)]
        filtered_frags = [frag for sublist in filtered_frags for frag in sublist]
        if (verbose == True):
            print("\nTotal cleavage products:", len(filtered_frags))

        # Asses unique fragments only.
        filtered_frags = list(set(filtered_frags))
        if (verbose == True):
            print("\nTotal unique cleavage products:", len(filtered_frags))

        # Filter by length.
        if (min_len != None):
            filtered_frags = [frag for frag in filtered_frags if (len(frag) >= min_len)]
        if (max_len != None):
            filtered_frags = [frag for frag in filtered_frags if (len(frag) <= max_len)]
        if (verbose == True):
            print("\nTotal unique cleavage products >= {} and <= {} residues:".format(min_len, max_len), 
                  len(filtered_frags), "\n")

        return filtered_frags
    

    def cleave_post_hoc(self,
                        seq_list, 
                        sites,
                        predictions, 
                        probabilities, 
                        confidence = 0.6):

        '''
        This method allows for post hoc cleavage based on prediction
        confidence. This is to be performed after results from 
        execute_pipeline() are returned.

        Parameters:
            seq_list: iterable of amino acid sequences as as one-letter code 
                strings.
            sites: iterable of potential cleavage sites as strings.
            predictions: iterable of predictions returned by panCleave.
            probabilities: iterable of class membership probability estimates
                returned by panCleave.
            confidence (default = 0.6): minimum threshold for probability
                estimates.

        Return: 
            List of protein fragments as one-letter code strings.
        '''

        # Instantiate fragmenter object.
        from fragment import Fragmenter
        frag = Fragmenter()

        # Cleave fragments using predicted label and probability (if relevant).
        fragments = []
        for i in range(len(seq_list)):
            if (("Positive" not in predictions[i]) or (len(seq_list[i]) < 8)):
                cryptides = None
            else:
                site_filter = [(item == "Positive") for item in predictions[i]]
                if (confidence != None):
                    pr_mask = [(item >= confidence) for item in probabilities[i]]
                    site_filter = [all(mask) for mask in zip(site_filter, pr_mask)]
                filtered_sites = [sites[i][j] for j in range(len(sites[i])) if site_filter[j]]
                cryptides = frag.cleave_single(seq_list[i], filtered_sites)
                if (len(cryptides) == 0):
                    cryptides = None

            fragments.append(cryptides)

        return fragments
    
    
    def identify_precursors(self,
                            df, 
                            frag_list, 
                            precursor_column = "Sequence", 
                            frag_column = "Fragments",
                            imported = True):

        '''
        This method identifies the precursor proteins (contained in a 
        dataframe) of protein fragments (contained in an interable structure,
        e.g. a list).

        Note: This method requires the following importation:
            `from ast import literal_eval`

        Parameters:
            df: pandas dataframe containing a column of precursor proteins and
                a column of fragments, as predicted by panCleave.
            frag_list: a list of fragments that need to be matched to their
                precursor protein(s). Each element of frag_list will be tested
                for membership in each cell of frag_column.
            precursor_column (default = "Sequence"): string indicating name of
                the dataframe column that contains precursor protein sequences.
            frag_column (default = "Fragments"): string indicating name of the
                dataframe column that contains the predicted fragments, as
                outputted by panCleave. Each cell should contain a list structure.
            imported (default = True): boolean indicating whether the dataframe
                has been imported (e.g. by pandas.read_csv()) (True) or generated
                at time of executing this method (False). The value of this
                parameter dictates whether or not to call ast.literal_eval().

        Return:
            precursor_df: pandas dataframe with one column for fragments and another 
                for their corresponding precursor proteins.
        '''

        # Init fragment:precursorProtein dictionary.
        precursor_dict = dict()

        for fragment in frag_list:
            for i in range(len(df)):

                # Test for need to literally evaluate data structure.
                if (imported):
                    current_frags = literal_eval(df[frag_column][i])
                else:
                    current_frags = df[frag_column][i]

                # Cross-reference fragment against each precursor's fragment list.
                if (fragment in current_frags):

                    # Test whether key is already in dictionary.
                    if fragment not in precursor_dict:
                        precursor_dict[fragment] = [df[precursor_column][i]]
                    else:
                        value = precursor_dict.get(fragment)
                        value.append(df[precursor_column][i])
                        precursor_dict[fragment] = value

        # Construct dataframe from dictionary.
        precursor_df = pd.Series(precursor_dict)
        precursor_df = pd.DataFrame(precursor_df).reset_index()
        precursor_df.columns = ["Fragment", "Precursors"]

        return precursor_df


    def merge_predictions(self,
                          df, 
                          columns, 
                          imported = True):

        '''
        This method merges multiple columns of fragment lists 
        produced by panCleave into one column, whose cells are
        a single comprehensive list.

        Note: This method requires the following importation:
            `from ast import literal_eval`

        Parameters:
            df: pandas dataframe containing multiple columns of
                panCleave predictions.
            columns: list of strings indicating column headers of
                fragment columns to merge.
            imported (default = True): boolean indicating whether 
                the dataframe has been imported (e.g. by pandas.read_csv()) 
                (True) or generated at time of executing this method 
                (False). The value of this parameter dictates whether 
                or not to call ast.literal_eval().

        Return: initial dataframe updated to contain a new column
            of merged fragment predictions.
        '''

        # Test for need to literally evaluate dataframe.
        if (imported):
            for i in range(len(columns)):
                df[columns[i]] = df[columns[i]].apply(lambda x: literal_eval(str(x)))

        # Create merged column.
        df["Fragments merged"] = df[columns[0]] + df[columns[1]]
        for i in range(2, len(columns)):
            df["Fragments merged"] = df["Fragments merged"] + df[columns[i]]

        return df