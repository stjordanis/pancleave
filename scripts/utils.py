import pandas as pd
from ast import literal_eval
from Bio import SeqIO

class Utils:
    
    
    '''
    Objects of this class provide broadly applicable utility methods.
    '''

    
    def generate_fasta(self,
                       seq_list, 
                       name_list = None, 
                       file_name = "seqs.fasta"):

        '''
        This method generates a FASTA file from a list of sequences
        and provided sequence names.

        Parameters:
            seq_list: iterable of sequence strings.
            name_list (default = None): iterable of sequence names. If value
                is None, a default list ["seq1", "seq2"...] will be used.
            file_name (default  = "seqs.fasta"): string indicating name 
                of generated FASTA file.

        Return: none.
        '''

        # Generate default name list.
        if (name_list == None):
            name_list = [("seq" + str(i)) for i in range(len(seq_list))]

        # Open and write to file.
        ofile = open(file_name, "w")
        for i in range(len(seq_list)):
            ofile.write(">" + name_list[i] + "\n" + seq_list[i] + "\n")
        ofile.close()
        
        
    def fasta_to_df(self,
                    file_path, 
                    verbose = False):
    
        '''
        This method converts a FASTA file to a dataframe.

        Note: this method requires biopython. When importing, specify:
            `from Bio import SeqIO`

        Parameters:
            file_path: string indicating the file path of the FASTA file.
            verbose (default = False): boolean indicating whether to
                print self-check statements.

        Return: pandas dataframe with a column for sequence IDs and a column
            for sequence strings.
        '''

        fasta_list = list(SeqIO.parse(file_path, "fasta"))

        # Extract IDs and sequences.
        id_list = [entry.id for entry in fasta_list]
        seq_list = [str(entry.seq) for entry in fasta_list]

        # Explore FASTA data, if specified.
        if (verbose):
            print("Preview full entry:\n", fasta_list[0], "\n")
            print("Preview ID only:\n", fasta_list[0].id, "\n")
            print("Preview sequence only:\n", fasta_list[0].seq)

            print("\n", id_list[:5], "\n")
            print("\n", seq_list[:5], "\n")

        # Construct dataframe.
        df = pd.DataFrame(list(zip(id_list, seq_list)), 
                          columns = ["ID", "Sequence"])

        return df
        

    def generate_single_ID(self,
                           fragment, 
                           precursor_ID):

        '''
        This method generates an ID for a protein fragment of the form
        precursorID-first3Length, e.g. D4IH25-MTA08 (D4IH25 = precursor
        protein UniProt ID, MTA = first 3 residues of fragment, 8 = fragment
        length).

        Parameters:
            fragment: string representing a protein fragment in one-letter code.
            precursor_ID: either a UniProt or NCBI protein ID representing the
                protein from which the given fragment derives.

        Return: fragment ID as string.
        '''

        if (len(fragment) < 10):
            length = "0" + str(len(fragment))
        else:
            length = str(len(fragment))

        return str(precursor_ID) + "-" + fragment[:3] + length


    def generate_IDs(self,
                     df, 
                     fragment_column = "Fragment sequence", 
                     precursor_column = "UniProt ID"):

        '''
        This method generates a column of IDs for a column of protein 
        fragments. IDs are of the form precursorID-first3Length, e.g. 
        D4IH25-MTA08 (D4IH25 = precursor protein UniProt name, MTA = first 
        3 residues of fragment, 8 = fragment length).

        Helper methods:
            generate_single_ID(fragment, precursor_ID)

        Parameters:
            df: pandas dataframe on which to operate.
            fragment_column: string indicating the name of the column
                containing protein fragment strings in one-letter code.
            precursor_column: string indicating the name of the column
                containing either a UniProt or NCBI protein ID representing 
                the protein from which the given fragment derives.

        Return: df modified to contain a column of fragment IDs as strings.
        '''

        df["Fragment ID"] = df.apply(lambda row: self.generate_single_ID(row[fragment_column],
                                                                         row[precursor_column]), 
                                     axis = 1)

        return df
    

    def remove_subseqs_internal(self,
                                df, 
                                column_name = "Sequence", 
                                verbose = False):

        '''
        This method removes sequences from a dataframe that are subsequences
        of other sequences in the same collection of sequences.
        
        NOTE: Do not confuse this method with remove_subseqs_external(). This 
        method compares sequences against other sequences within the same data 
        structure, e.g. the same dataframe column (internal). The method 
        remove_subseqs_external() compares sequences in one data structure to 
        sequences in a separate (external) data structure.

        Parameters:
            df: pandas dataframe with all sequence strings in a single column.
            column_name (default = "Sequence"): string indicating name of column
                containing sequence strings.
            verbose (default = False): boolean indicating whether to print
                subsequences identified.

        Return: filtered dataframe.
        '''

        # Identify peptides that are subsquences of other peptides.
        # Keep whichever sequence is in desired length range (8-20aa), else keep second.
        remove_indices = set()
        seq_list = list(df[column_name])
        for i in range(len(seq_list)):
            for j in range(len(seq_list)):
                if (i != j and seq_list[i] in seq_list[j]):
                    if (len(seq_list[i]) in range(8, 21) and len(seq_list[j]) not in range(8, 21)):
                        remove_indices.add(i)
                    elif (len(seq_list[j]) in range(8, 21) and len(seq_list[i]) not in range(8, 21)):
                        remove_indices.add(j)
                    else:
                        remove_indices.add(i)
                    if (verbose == True):
                        print(seq_list[i], "in", seq_list[j])

        print("\nTotal sequences to remove =", len(remove_indices), "\n")

        # Remove peptides and return.
        return df.drop(index = remove_indices).reset_index(drop = True)


    def remove_subseqs_external(self,
                                df, 
                                seq_list,
                                column_name = "Sequence", 
                                verbose = False):

        '''
        This method removes sequences from a dataframe that are subsequences
        of other sequences contained in an iterable data structure.

        NOTE: Do not confuse this method with remove_subseqs_internal(). This 
        method compares sequences in one data structure to sequences in a separate 
        (external) data structure. The method remove_subseqs_internal() compares 
        against sequences within the same data structure, e.g. the same dataframe 
        column (internal).

        Parameters:
            df: pandas dataframe with all sequence strings in a single column. 
                df[column_name] might contain sequences that are subsequences of 
                sequences found in seq_list.
            seq_list: iterable containing sequences that might contain subsequences
                identical to those in df[column_name].
            column_name (default = "Sequence"): string indicating name of column
                containing sequence strings.
            verbose (default = False): boolean indicating whether to print
                subsequences identified.

        Return: filtered dataframe.
        '''

        # Identify peptides that are subsquences of modern human peptides.
        remove_indices = set()
        for i in range(len(df[column_name])):
            for j in range(len(seq_list)):
                if (df[column_name][i] in seq_list[j]):
                    remove_indices.add(i)
                    if (verbose == True):
                        print(df[column_name][i], "in", seq_list[j])
                    break

        print("\nTotal sequences to remove =", len(remove_indices), "\n")

        # Remove peptides and return.
        return df.drop(index = remove_indices).reset_index(drop = True)