import unittest
from pipeline import Pipeline


class TestPipeline(unittest.TestCase):
    
    
    def test_execute_pipeline(self):
        
        # Instantiate Pipeline object.
        pipe = Pipeline()
        seq_list_mini = ["GEKLKKIGKKIKNFFQKL"]
        
        # Test little dataframe.
        df_mini_test = pipe.execute_pipeline(seq_list_mini)
        correct = ["GEKL", "K", "K", "IG", "K", "KIKN", "FFQKL"]
        self.assertEqual(df_mini_test.Fragments[0], correct)
        
    
if (__name__ == "__main__"):
    unittest.main()